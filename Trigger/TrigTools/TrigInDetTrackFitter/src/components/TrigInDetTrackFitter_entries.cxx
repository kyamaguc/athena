#include "../TrigInDetTrackFitter.h"
#include "../TrigDkfTrackMakerTool.h"
#include "../TrigL2ResidualCalculator.h"

DECLARE_COMPONENT( TrigInDetTrackFitter )
DECLARE_COMPONENT( TrigDkfTrackMakerTool )
DECLARE_COMPONENT( TrigL2ResidualCalculator )
